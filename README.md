# TATA projects repository #

### Version: 0.1 Alpha ###

### Apie projektą: ###

* Projektų valdymo sistema skirta palaikyti pagal Scrum metodologiją vykdomų projektų procesą.
* Įmonei, įsidiegusiai programinę įranga, suteikiama galimybė registruoti projektus ir sekti jų įgyvendinimą. 
* Projektai sistemoje dalinami į sprintus, kurių kiekvienas sudarytas iš užduočių sąrašo. 
* Sistema seka sprinto bei projekto progresą pateikdama įvairias projekto vykdytojams aktualias metrikas. 
* Projekto vykdytojai gali diskutuoti apie užduotis sistemoje, taip saugodami visą su užduoties vykdymu susijusią informaciją vienoje vietoje. 
* Užduotys taip pat gali būti vertinamos, sekamas jų statusas.