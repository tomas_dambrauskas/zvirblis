package lt.akademija.models;

import java.util.Collection;
import javax.validation.Valid;

import lt.akademija.entities.Project;

public class ProjectModel {

	private Project project;
	@Valid
	private Project currentProject;

	private Collection<Project> projects;

	public void createProject() {
		project = new Project();
	}

	public void nullProject() {
		project = null;
	}

	public Collection<Project> getProjects() {
		return projects;
	}

	public void setProjects(Collection<Project> projects) {
		this.projects = projects;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Project getCurrentProject() {
		return currentProject;
	}

	public void setCurrentProject(Project currentProject) {
		this.currentProject = currentProject;
	}

}
