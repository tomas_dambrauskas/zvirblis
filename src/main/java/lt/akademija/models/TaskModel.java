package lt.akademija.models;

import java.util.List;

import lt.akademija.entities.Task;

public class TaskModel {
	
	private Task selectedTask;
	private List<Task> tasks;
	
	public Task getSelectedTask() {
		return selectedTask;
	}
	
	public void setSelectedTask(Task selectedTask) {
		this.selectedTask = selectedTask;
	}
	
	public List<Task> getTasks() {
		return tasks;
	}
	
	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
	
	
}
