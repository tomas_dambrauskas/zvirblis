package lt.akademija.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import lt.akademija.entities.Task;

public class TaskRepository {

	private EntityManagerFactory entityManagerFactory;

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}
	
	public EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
	
	public Task findByTaskName(String taskName) {
		EntityManager entityManager = getEntityManager();
		try {
			TypedQuery<Task> authorQuery = entityManager.createQuery("SELECT k From Task k WHERE k.id = :id", Task.class);
			authorQuery.setParameter("taskName", taskName);
			authorQuery.setMaxResults(1);
			return authorQuery.getSingleResult();
		} finally {
			entityManager.close();
		}
	}
	
	public void save(Task task) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			if(!entityManager.contains(task))
				task = entityManager.merge(task);
			entityManager.persist(task);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}
	
	public void delete(Task task) {
		EntityManager entityManager = getEntityManager();
		try {
			entityManager.getTransaction().begin();
			task = entityManager.merge(task);
			entityManager.remove(task);
			entityManager.getTransaction().commit();
		} finally {
			entityManager.close();
		}
	}
	
	public List<Task> getAllTasks() {
		EntityManager entityManager = getEntityManager();
		try {
			TypedQuery<Task> authorQuery = entityManager.createQuery("SELECT t FROM Task t", Task.class);
			return authorQuery.getResultList();
		} finally {
			entityManager.close();
		}
	}
	
	
}
