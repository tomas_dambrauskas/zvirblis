package lt.akademija.repositories;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import lt.akademija.entities.Project;

public class ProjectDao {

	private EntityManagerFactory entityManagerFactory;
	private EntityManager em;

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		this.em = entityManagerFactory.createEntityManager();
	}

	public void create(Project project) {
		em = entityManagerFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			if (!em.contains(project))
				project = em.merge(project);
			em.persist(project);
			em.getTransaction().commit();
		} finally {
			em.close();
		}
	}

	public Collection<Project> findAll() {
		em = entityManagerFactory.createEntityManager();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Project> cq = cb.createQuery(Project.class);
			Root<Project> root = cq.from(Project.class);
			cq.select(root); // we select entity here
			TypedQuery<Project> q = em.createQuery(cq);
			return q.getResultList();
		} finally {
			em.close();
		}
	}

	public void save(Project newProject) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		if (!em.contains(newProject))
			newProject = em.merge(newProject);
		em.persist(newProject);
		em.getTransaction().commit();
		em.close();
	}

	public void delete(Project project) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		project = em.merge(project);
		em.remove(project);
		em.getTransaction().commit();
		em.close();
	}

	public Project findByProjectName(String projectName) {
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Project> authorQuery = em.createQuery("SELECT t From Train t WHERE t.id = :id", Project.class);
		authorQuery.setParameter("projectName", projectName);
		authorQuery.setMaxResults(1);
		return authorQuery.getSingleResult();

	}

}
