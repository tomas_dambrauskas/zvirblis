package lt.akademija.controllers;

import java.util.Collection;

import lt.akademija.entities.Project;
import lt.akademija.models.ProjectModel;
import lt.akademija.repositories.ProjectDao;

public class ProjectController {

	private ProjectModel projectModel;
	private ProjectDao projectDao;

	public Collection<Project> getProjectList() {
		return projectDao.findAll();
	}

	public String create() {
		projectModel.setCurrentProject(new Project());
		return "project-create";
	}

	public String update(Project project) {
		projectModel.setCurrentProject(project);
		return "project-create";
	}

	public String cancel() {
		projectModel.setCurrentProject(null);
		return "project-list";
	}

	public String save() {
		projectDao.save(projectModel.getCurrentProject());
		return "project-list";
	}

	public Project findByProjectName(String projectName) {
		return projectDao.findByProjectName(projectName);
	}

	public void delete(Project project) {
		projectDao.delete(project);
	}

	public String overview(Project project) {
		projectModel.setCurrentProject(project);
		return "project-overview";
	}

	public ProjectModel getProjectModel() {
		return projectModel;
	}

	public void setProjectModel(ProjectModel projectModel) {
		this.projectModel = projectModel;
	}

	public ProjectDao getProjectDao() {
		return projectDao;
	}

	public void setProjectDao(ProjectDao projectDao) {
		this.projectDao = projectDao;
	}

}
