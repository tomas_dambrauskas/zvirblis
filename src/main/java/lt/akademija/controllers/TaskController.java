package lt.akademija.controllers;

import java.util.List;

import lt.akademija.entities.Task;
import lt.akademija.models.TaskModel;
import lt.akademija.repositories.TaskRepository;

public class TaskController {

	private TaskRepository taskRepository;
	private TaskModel taskModel;
	
	public TaskRepository getTaskRepository() {
		return taskRepository;
	}
	
	public void setTaskRepository(TaskRepository taskRepository) {
		this.taskRepository = taskRepository;
	}
	
	public TaskModel getTaskModel() {
		return taskModel;
	}
	
	public void setTaskModel(TaskModel taskModel) {
		this.taskModel = taskModel;
	}
	
	public List<Task> getTaskList() {
		return taskRepository.getAllTasks();
	}
	
	public void add() {
		taskModel.setSelectedTask(new Task());
	}
	
	public void update(String taskName) {
		taskModel.setSelectedTask(taskRepository.findByTaskName(taskName));
	}
	
	public void save() {
		taskRepository.save(taskModel.getSelectedTask());
	}
	
	public void delete(Task task) {
		taskRepository.delete(task);
	}
	
	public void cancel() {
		taskModel.setSelectedTask(new Task());
	}
	
	
}
